<?php
function dna_sequence($sequence_1, $sequence_2)
{
    $split_sequence1 = str_split($sequence_1);
    $split_sequence2 = str_split($sequence_2);
    $position1 = -1;
    $position2 = -1;
    $length = 0;

    foreach ($split_sequence1 as $key => $value) {
        if (!in_array($value, $split_sequence2)) {
            $position1 = !($position1 > -1) ? -1 : $position1;
            continue;
        }

        if ( $length >= sizeof($split_sequence2)) {
            continue;
        }

        if (($key > 0 || $key < (sizeof($split_sequence1) - 1)) && (in_array($split_sequence1[$key + 1],
                    $split_sequence2) || in_array($split_sequence1[$key - 1], $split_sequence2))) {
            $length++;
            if ($position1 === -1 ) {
                $position1 = $key;
            }
        }
    }
    $position2 = $position1 === -1 ? -1 : 0;

    return [$length, $position1, $position2];
}

// Test 1
//echo print_r(dna_sequence("ACGT", "GTC"), true);
// Test 2
//echo print_r(dna_sequence("ACGT", "CAT"), true);
// Test 3
//echo print_r(dna_sequence("ACA", "AC"), true);
// Test 4
//echo print_r(dna_sequence("ACA", "GT"), true);

// Own Testing:
//echo print_r(dna_sequence("ACGTACA", "GTA"), true);